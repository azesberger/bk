
CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

insert into book (userId, value) values (123, 50);
insert into book (userId, value) values (123, -1);
insert into book (userId, value) values (123, -1);
insert into book (userId, value) values (123, -1);
insert into book (userId, value) values (123, -1);
insert into book (userId, value) values (123, -1);
insert into book (userId, value) values (217, 10);
insert into book (userId, value) values (217, -1);
insert into book (userId, value) values (217, -1);
insert into book (userId, value) values (217, -1);

SELECT SUM(value) FROM book;



 