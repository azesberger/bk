package login;

import java.io.IOException;
import java.security.MessageDigest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.Base64Coder;
import db.DBConnection;

/**
 * Servlet implementation class CreateAccount
 */
@WebServlet("/CreateAccount")
public class CreateAccount extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateAccount() {

		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBConnection db = null;
		try {
			String cmd = request.getParameter("cmd");
			String name = request.getParameter("name");
			String pwd = request.getParameter("pwd");
			String pwd2 = request.getParameter("pwd2");
			String key = request.getParameter("key");
			boolean keepLoggedIn = "on".equals(request.getParameter("keeploggedin"));
			boolean changePwd = "on".equals(request.getParameter("changepwd"));
			String pwdNew = request.getParameter("pwdnew");
			String pwdNewRepeat = request.getParameter("pwdnewrepeat");
			HttpSession session = request.getSession(true);

			UserManager userMgr = new UserManager();

			if ("logout".equals(cmd)) {
				userMgr.logout((Integer) session.getAttribute("userId"));
				session.setAttribute("userId", null);
				Cookie c = new Cookie("bierkassa", null);
				response.addCookie(c);
				return;
			}
			if ((name == null) || (name.equals("")))
				throw new RuntimeException("name is empty");
			if ((pwd == null) || (pwd.equals("")))
				throw new RuntimeException("password empty");

			String pwdHash = encrypt(pwd);

			if (key != null) { // create account
				if ((!pwd.equals(pwd2)) || (pwd.trim().isEmpty()))
					throw new RuntimeException("passwords different or empty");

				userMgr.createAccount(name, pwdHash, key);
			}

			if (changePwd) {
				if ((!pwdNew.equals(pwdNewRepeat)) || (pwdNew.trim().isEmpty()))
					throw new RuntimeException("passwords different or empty");
			}

			int userId = userMgr.login(name, pwdHash, keepLoggedIn ? session.getId() : "");
			if (changePwd)
				userMgr.changePwd(userId, encrypt(pwdNew));
			Cookie c = new Cookie("bierkassa", keepLoggedIn ? session.getId() : null);
			c.setMaxAge(3600 * 24 * 365);
			response.addCookie(c);

			session.setMaxInactiveInterval(3600);
			session.setAttribute("userId", userId);

			String s = "";
			s = request.getParameter("prepaykey");
			if (s != null && !s.equals(""))
				s = "?prepaykey=" + s;
			else
				s = "";
			String rd = response.encodeRedirectURL("card.html" + s);
			System.out.println("Session " + request.isRequestedSessionIdFromCookie() + " " + session.getId() + " / "
					+ session.getMaxInactiveInterval() + " / " + rd);
			response.sendRedirect(rd);
		} catch (Exception e) {
			response.getWriter().println(e.getMessage());
			e.printStackTrace();
		} finally {
			if (db != null)
				db.close();
		}
	}

	private String encrypt(String pwd) {
		try {
			byte[] bytesOfMessage = pwd.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			return new String(Base64Coder.encode(thedigest));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
