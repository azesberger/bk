package login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import db.DBConnection;
import mail.Mail;

public class UserManager {

	public UserManager() {

	}

	public void createAccount(String name, String pwdHash, String key) {
		DBConnection db = null;
		try {
			db = new DBConnection();
			checkKey(db, key);
			PreparedStatement s = db.getConnection().prepareStatement("INSERT INTO users (name, pwd) VALUES (?, ?);");
			s.setString(1, name);
			s.setString(2, pwdHash);
			s.execute();
			new Mail().send("Infomail: New Account created", "New account created:\r\nusername: " + name);
		} catch (SQLException e) {
			throw new RuntimeException("create account failed: " + e.getMessage(), e);
		} finally {
			if (db != null)
				db.close();
		}

	}

	public int login(String keepLoggedInData) {
		DBConnection db = null;
		try {
			db = new DBConnection();

			PreparedStatement s = db.getConnection().prepareStatement("SELECT userId FROM users where keepLoggedIn=?;");
			s.setString(1, keepLoggedInData);

			ResultSet rs = s.executeQuery();
			if (!rs.next())
				throw new RuntimeException("invalid user or password!");

			return rs.getInt(1);

		} catch (SQLException e) {
			throw new RuntimeException("login failed: " + e.getMessage(), e);
		} finally {
			if (db != null)
				db.close();
		}
	}

	public int login(String name, String pwdHash, String keepLoggedInData) {
		DBConnection db = null;
		try {
			db = new DBConnection();

			PreparedStatement s = db.getConnection().prepareStatement(
					"SELECT userId,keepLoggedIn,name FROM users where name=? and pwd=?;", ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);
			s.setString(1, name);
			s.setString(2, pwdHash);
			ResultSet rs = s.executeQuery();
			if (!rs.next())
				throw new RuntimeException("invalid user or password!");
			rs.updateString(2, keepLoggedInData);
			rs.updateRow();
			return rs.getInt(1);

		} catch (SQLException e) {
			throw new RuntimeException("login failed: " + e.getMessage(), e);
		} finally {
			if (db != null)
				db.close();
		}

	}

	public void logout(int userId) {
		DBConnection db = null;
		try {
			db = new DBConnection();

			PreparedStatement s = db.getConnection().prepareStatement(
					"SELECT keepLoggedIn,name FROM users where userId=?", ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE);
			s.setInt(1, userId);

			ResultSet rs = s.executeQuery();
			if (!rs.next())
				throw new RuntimeException("invalid user!");
			rs.updateString(1, "");
			rs.updateRow();

		} catch (SQLException e) {
			throw new RuntimeException("login failed: " + e.getMessage(), e);
		} finally {
			if (db != null)
				db.close();
		}

	}

	private void checkKey(DBConnection db, String key) {
		try {
			PreparedStatement s = db.getConnection()
					.prepareStatement("SELECT data FROM keytable WHERE (active=1 AND type='createaccount');");

			ResultSet rs = s.executeQuery();

			if (rs.next() && rs.getString(1).equals(key))
				return;
			throw new RuntimeException("invalid key specified - not allowed to create an account");
		} catch (SQLException e) {
			throw new RuntimeException("keycheck failed", e);
		}

	}

	public void changePwd(int userId, String pwd) {
		DBConnection db = null;
		try {
			db = new DBConnection();

			PreparedStatement s = db.getConnection().prepareStatement("SELECT pwd,name FROM users where userId=?",
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
			s.setInt(1, userId);

			ResultSet rs = s.executeQuery();
			if (!rs.next())
				throw new RuntimeException("invalid user!");
			rs.updateString(1, pwd);
			rs.updateRow();

		} catch (SQLException e) {
			throw new RuntimeException("change pwd failed: " + e.getMessage(), e);
		} finally {
			if (db != null)
				db.close();
		}

	}

	// public void xx() {
	//
	// Integer userId = (Integer) session.getAttribute("userId"); boolean
	// keepLoggedIn = "on".equals(request.getParameter("keeploggedin"));
	//
	// if (userId == null) { String keepLoginData = ""; for (Cookie c :
	// request.getCookies()) { if (c.getName().equals("bierkassa")) {
	// keepLoginData = c.getValue(); } } if (!keepLoginData.equals("")) {
	// PreparedStatement s = db.getConnection() .prepareStatement(
	// "SELECT userId FROM users where keepLoggedIn=?;"); s.setString(1,
	// keepLoginData);
	//
	// ResultSet rs = s.executeQuery();
	//
	// if (rs.next()) userId = rs.getInt(1);
	//
	// } } if (userId != null) session.setAttribute("userId", new
	// Integer(userId)); else { if (name == null) return; // login UserData
	// userData = login(db, name, pwdHash);
	// session.setMaxInactiveInterval(3600); session.setAttribute("userId",
	// new Integer(userData.userId));
	//
	// if (keepLoggedIn != userData.keepLoggedIn()) { if (keepLoggedIn) {
	// userData.keepLoggedInData = session.getId(); Cookie c = new
	// Cookie("bierkassa", session.getId()); c.setMaxAge(3600 * 24 * 365);
	// c.setComment("bierkassa"); response.addCookie(c); } else {
	// userData.keepLoggedInData = ""; response.addCookie(new
	// Cookie("bierkassa", "")); } // ginge in einem mit login ...
	// PreparedStatement ps = db.getConnection() .prepareStatement(
	// "UPDATE users SET keepLoggedIn = ? WHERE userId = ?;");
	// ps.setString(1, userData.keepLoggedInData); ps.setInt(2,
	// userData.userId); ps.executeUpdate(); }
	//
	// }
}