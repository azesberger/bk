package admin;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.DBConnection;

/**
 * Servlet implementation class ResetPwd
 */
@WebServlet("/ResetPwd")
public class ResetPwd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResetPwd() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null) {

			response.sendError(HttpServletResponse.SC_FORBIDDEN, "not logged in");
			return;
		}
		int userId = (Integer) session.getAttribute("userId");
		int userIdReset = Integer.parseInt(request.getParameter("userId"));

		DBConnection db = null;
		try {
			db = new DBConnection(DBConnection.Admin, userId);
			boolean b = false;
			try {
				PreparedStatement s = db.getConnection().prepareStatement("update users set pwd=? where userId=?");
				s.setString(1, "ESXUXQpC+qLuzWHMNNCCEQ==");
				s.setInt(2, userIdReset);
				s.execute();
				b = true;
			} catch (SQLException e) {
				e.printStackTrace();
			}

			response.getWriter().println("</head>");
			response.getWriter().println("<body>");
			if (b)
				response.getWriter().println("<p>reset pwd for user " + userIdReset + "</p>");
			else
				response.getWriter().println("<p>reset pwd for user " + userIdReset + " failed</p>");
			response.getWriter().println("</body>");
			response.getWriter().println("</html>");

		} finally

		{
			if (db != null) {

				db.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
