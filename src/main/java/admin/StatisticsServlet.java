
/*
bier per day
SELECT convert(timeStamp,date), SUM( book.value ) 
FROM book
WHERE TYPE = "consume" group by convert(timeStamp, DATE)



gewonnene biere
SELECT users.userId, users.name, sum(book.value) FROM book  join users on book.userId=users.userId join keytable on keytable.data=book.keyData where keytable.type="promotionkey" group by users.name  order by sum(book.value) desc

user stat. group day
SELECT convert( timeStamp, date ), 
SUM(if(book.value<0,-book.value,0)) AS "out",
SUM(if(book.value>0,book.value,0)) AS "in"      
FROM book
where userId =1
GROUP BY convert( timeStamp, DATE ) 

SELECT yearweek( timeStamp )
year

SELECT convert( timeStamp, date ) , 
SUM( if( book.value <0, - book.value, 0 ) ) AS "out", 
SUM( if( book.value >0, book.value, 0 ) ) AS "in", 
SUM( if( TYPE = "prepay", 1, 0 ) ) AS "euro"
FROM book
WHERE userId =1
GROUP BY convert( timeStamp, DATE ) 

SELECT convert( timeStamp, date ) , 
SUM( if( book.value <0, - book.value, 0 ) ) AS "out", 
SUM( if( book.value >0, book.value, 0 ) ) AS "in", 
SUM( if( book.value >0, (SELECT price from keytable join book on keytable.data=book.keyData), 0 ) ) AS "euro"
FROM book
WHERE userId =1
GROUP BY convert( timeStamp, DATE ) 
*/
package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.DBConnection;

/**
 * Servlet implementation class Statistics
 */
@WebServlet("/Statistics")
public class StatisticsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StatisticsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session == null) {

			response.sendError(HttpServletResponse.SC_FORBIDDEN, "not logged in");
			return;
		}
		int userID = (Integer) session.getAttribute("userId");

		DBConnection db = new DBConnection(DBConnection.Admin, userID);

		try {

			int all = 0;
			int month = 0;
			int week = 0;
			int day = 0;

			PreparedStatement s = db.getConnection()
					.prepareStatement("SELECT sum(book.value) FROM book where type='consume'");
			ResultSet rs = s.executeQuery();

			if (rs.next())
				all = rs.getInt(1);

			s = db.getConnection()
					.prepareStatement("SELECT sum(book.value) FROM book where type='consume' and timeStamp>=?");

			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
			c.setTimeInMillis(System.currentTimeMillis());
			c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), 0, 0, 0);
			java.sql.Timestamp d = new java.sql.Timestamp(c.getTimeInMillis());

			s.setTimestamp(1, d, c);
			rs = s.executeQuery();
			System.out.println(d);
			if (rs.next())
				day = rs.getInt(1);

			///
			c.setTimeInMillis(System.currentTimeMillis());
			c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), 0, 0, 0);
			c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			long timeOld = d.getTime();
			d = new java.sql.Timestamp(c.getTimeInMillis());
			if (d.getTime() > timeOld)
				d = new Timestamp(d.getTime() - 7 * 24 * 3600 * 1000);
			System.out.println(d);
			s.setTimestamp(1, d, c);
			rs = s.executeQuery();
			if (rs.next())
				week = rs.getInt(1);

			c.setTimeInMillis(System.currentTimeMillis());
			c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), 0, 0, 0);
			c.set(Calendar.DAY_OF_MONTH, 1);
			d = new java.sql.Timestamp(c.getTimeInMillis());
			System.out.println(d);
			s.setTimestamp(1, d, c);
			rs = s.executeQuery();
			if (rs.next())
				month = rs.getInt(1);

			response.getWriter().println(
					"beerconsume <br>all " + all + " <br>month " + month + " <br>week " + week + " <br>day " + day);
			response.getWriter().println("<table>");
			response.getWriter()
					.print("	<tr>" + "  <th>userid</th>" + "   <th>name</th> " + "  <th>sum</th>" + "  </tr>");

			s = db.getConnection().prepareStatement(
					"SELECT users.userId, users.name, sum(book.value) FROM book  join users on book.userId=users.userId group by users.name  order by users.userId");

			rs = s.executeQuery();

			while (rs.next())
				printRow(response.getWriter(), rs);

			s = db.getConnection().prepareStatement("SELECT sum(value) FROM book");
			rs = s.executeQuery();
			rs.next();
			response.getWriter().println("<tr><td>sum all:</td><td></td><td> " + rs.getInt(1) + "</td></tr>");
			// ----
			s = db.getConnection().prepareStatement(
					"select price,count(price),sum(price) from keytable where active=1 and type='prepaidkey' group by price order by price");
			rs = s.executeQuery();
			response.getWriter()
					.print("	<tr>" + "  <th>price</th>" + "   <th>pcs</th> " + "  <th>sum</th>" + "  </tr>");

			while (rs.next())
				printRow(response.getWriter(), rs);

			response.getWriter().println("</table>");
			// ----
			s = db.getConnection().prepareStatement(
					"select count(id) from keytable where type='promotionkey' group by active order by active");
			rs = s.executeQuery();

			int inactive = 0;
			int active = 0;
			if (rs.next())
				inactive = rs.getInt(1);
			if (rs.next())
				active = rs.getInt(1);
			response.getWriter().println("<p>promokeys: " + active + "/" + (active + inactive) + " </p>");

			s = db.getConnection().prepareStatement("select * from online");
			rs = s.executeQuery();
			response.getWriter().print("<p>online</p><table>	<tr>" + "  <th>name</th>" + "   <th>time</th></tr> ");

			while (rs.next()) {
				Timestamp ts = rs.getTimestamp(2);
				ts = new Timestamp(ts.getTime() + 6 * 3600 * 1000);

				response.getWriter().println("<tr><td>" + rs.getString(1) + "</td><td>" + ts + "</td></tr>");
			}

			response.getWriter().println("</table>");
		} catch (SQLException e) {
			response.getWriter().println(e);
		} finally {
			if (db != null)
				db.close();
		}

	}

	private void printRow(PrintWriter p, ResultSet rs) throws SQLException {
		p.println(" <tr>" + "  <td>" + rs.getInt(1) + "</td>" + "   <td>" + rs.getString(2) + "</td>" + "  <td>"
				+ rs.getInt(3) + "</td>" + " </tr>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
