package admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.Base64Coder;
import db.DBConnection;

/**
 * Servlet implementation class CreateCodesServlet
 */
@WebServlet("/CreateCodesServlet")
public class CreateCodesServlet extends HttpServlet {
	public static final int cCodesPerPage = 10;

	public class HtmlCodeWriter {

		private PrintWriter writer;
		private int cnt = 0;

		public HtmlCodeWriter(PrintWriter writer) {
			HtmlCodeWriter.this.writer = writer;
		}

		public void writeCode(String code, int price, int value, String col) {
			if ((cnt % 2) == 0) {
				if (cnt > 0)
					writeDivEnd("out");

				if ((cnt % cCodesPerPage) == 0) {
					if (cnt > 0)
						writeDivEnd("page");
					writeDivStart("page");
				}

				writeDivStart("out");
			}
			writeDivStart("inner");
			writeDivStart("back");

			writer.println(
					"<img src=\"images/back.jpg\" width = 100%><img class=\"code\" src=\"http://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor="
							+ col + "&amp;data=http%3A%2F%2Fbk-bkc.rhcloud.com%2Fcard.html%3Fprepaykey%3D" + code
							+ "&amp;qzone=2&amp;margin=0&amp;size=100x100&amp;ecc=L\" alt=\"qr code\" /></img>");

			writeDivEnd("back");
			writeDivStart("front");
			writer.println("<h1>Bierkassa Credit " + price + "&euro;</h1><h2>" + value + " Bier</h2><h3>Code: " + code
					+ "</h3><p>http://bk-bkc.rhcloud.com       Kontakt: bierkassa@gmx.at</p>");

			writeDivEnd("front");
			writeDivEnd("inner");
			cnt++;
		}

		private void writeDivEnd(String comment) {
			writer.println("</div>");

		}

		private void writeDivStart(String name) {
			writer.println("<div class=\"" + name + "\">");
		}

		public void writeStart(String userName) {
			writer.println("<!DOCTYPE html>");
			writer.println("<html>");
			writer.println("<head>");
			writer.println("<meta charset=\"ISO-8859-1\">");
			// set the Calendar of sdf to timezone T1

			SimpleDateFormat sdf = new SimpleDateFormat();
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(System.currentTimeMillis());
			sdf.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));

			writer.println("<title>Generated Codes by user" + userName + " (" + sdf.format(c.getTime()) + ")</title>");
			writer.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"codes.css\">");
			writer.println("</head>");
			writer.println("<body>");

		}

		public void writeEnd() {
			writeDivEnd("out");
			writeDivEnd("page");
			writer.println("</body>");
			writer.println("</html>");
			writer.flush();
		}

	}

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateCodesServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session == null) {

			response.sendError(HttpServletResponse.SC_FORBIDDEN, "not logged in");
			return;
		}
		int userID = (Integer) session.getAttribute("userId");
		String text = null;
		DBConnection db = null;
		try {
			db = new DBConnection(DBConnection.Admin, userID);

			int value = Integer.parseInt(request.getParameter("value"));
			int price = Integer.parseInt(request.getParameter("price"));
			int number = Integer.parseInt(request.getParameter("number"));
			boolean isPromo = "on".equals(request.getParameter("ispromo"));
			if (isPromo) {
				value = 0;
				price = 0;
				for (int i = 0; i < number; i++)
					try {
						this.addToDB(db, createEncodedID(), 1, 0, "promotionkey");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				response.getWriter().println("</head>");
				response.getWriter().println("<body><p> added promotion codes!</p>");

				response.getWriter().println("</body>");
				response.getWriter().println("</html>");
			} else {

				response.setContentType("text/html");
				HtmlCodeWriter codeWriter = new HtmlCodeWriter(response.getWriter());
				codeWriter.writeStart("---");

				generate(number, value, price, codeWriter, db);
				codeWriter.writeEnd();
			}

		} finally {
			if (db != null) {

				db.close();
			}
		}
	}

	private void generate(int number, int value, int price, HtmlCodeWriter writer, DBConnection db) {

		String col = "FF6528";
		if (price >= 50)
			col = "FFD800";
		else if (price >= 20) {
			col = "28DCED";
		}
		try {

			db.getConnection().setAutoCommit(false);
			for (int n = 0; n < number; n++) {
				String s = null;

				do {
					s = createEncodedID();

				} while (s.indexOf('/') >= 0 || s.indexOf('+') >= 0);
				addToDB(db, s, value, price);
				writer.writeCode(s, price, value, col);

			}
			db.getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	private void addToDB(DBConnection db, String code, int value, int price) throws SQLException {
		addToDB(db, code, value, price, "prepaidkey");
	}

	private void addToDB(DBConnection db, String code, int value, int price, String type) throws SQLException {
		PreparedStatement s = db.getConnection()
				.prepareStatement("INSERT INTO keytable (type, data, value, price) VALUES (?, ?, ?, ?);");
		s.setString(1, type);
		s.setString(2, code);
		s.setInt(3, value);
		s.setInt(4, price);

		s.executeUpdate();

	}

	public static String createEncodedID() {
		UUID u = UUID.randomUUID();

		long l1 = u.getLeastSignificantBits();
		long l2 = u.getMostSignificantBits();
		byte[] b = new byte[16];
		for (int i = 0; i < 8; i++) {
			b[i] = (byte) (l1 & 0xff);
			l1 = l1 >> 8;
		}
		for (int i = 8; i < 16; i++) {
			b[i] = (byte) (l2 & 0xff);
			l2 = l2 >> 8;
		}
		String res = new String(Base64Coder.encode(b));
		int pos = res.indexOf('=');
		if (pos >= 0)
			res = res.substring(0, pos);
		return res;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
