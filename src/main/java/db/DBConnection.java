package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection implements AutoCloseable {
	public static final String Admin = "Admin";
	private Connection conn;

	public DBConnection() {
		this(null, null);
	}

	public DBConnection(String role, Integer userID) {

		connect();
		if (role != null)
			checkRole(role, userID);
	}

	private void checkRole(String role, Integer userID) {
		try {
			PreparedStatement s = getConnection()
					.prepareStatement("SELECT name FROM users where userId=? AND role='" + role + "';");
			s.setInt(1, userID);

			ResultSet rs = s.executeQuery();
			if (rs.next())
				rs.getString(1);
			else
				throw new RuntimeException("not logged in");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private final void connect() {
		try {
			String HOST = System.getenv("MYSQL_SERVICE_HOST");
			String PORT = System.getenv("MYSQL_SERVICE_PORT");
			String USERNAME = System.getenv("MYSQL_USER");
			String PASSWORD = System.getenv("MYSQL_PASSWORD");

			System.out.println("user: " + USERNAME + " pass " + PASSWORD + " host" + HOST);

			if (HOST == null) {
				HOST = "localhost";
				PORT = "3306";
				USERNAME = "root";
				PASSWORD = "pass";
			}

			Properties connectionProps = new Properties();
			connectionProps.put("user", USERNAME);
			connectionProps.put("password", PASSWORD);

			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection("jdbc:mysql://" + HOST + ":" + PORT + "/bk", USERNAME, PASSWORD);
		} catch (Exception ex) {
			throw new RuntimeException("cannot create db connection", ex);
		}

	}

	public Connection getConnection() {
		return conn;
	}

	@Override
	public void close() {
		if (conn == null)
			return;

		try {
			conn.close();
		} catch (SQLException e) {

		}

		conn = null;
	}

}
