package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import db.DBConnection;
import login.UserManager;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");

		HttpSession session = request.getSession(true);
		System.out.println("session" + session + "/" + session.getId());
		Integer userID = (Integer) session.getAttribute("userId");
		if (userID == null) {

			try {
				for (Cookie c : request.getCookies()) {

					if ((c.getName().equals("bierkassa")) && (c.getValue() != null) && (!c.getValue().equals(""))) {
						userID = new UserManager().login(c.getValue());
						session.setAttribute("userId", userID);
						break;

					}
				}
			} catch (Exception e) {
				userID = null;
				e.printStackTrace();
			}
		}

		if (userID == null)

		{
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "not logged in");
			return;
		}

		System.out.println("Session " + request.isRequestedSessionIdFromCookie() + "  " + session.getId() + " / "
				+ session.getMaxInactiveInterval());

		JSONObject respObj = new JSONObject();
		respObj.put("version", versionInfo.VersionInfo.Version);
		DBConnection db = null;
		try {
			db = new DBConnection();
			// gach
			if (request.getParameter("contact") != null) {
				respObj.put("mail", "bierkassa@gmx.at");

				response.getWriter().println(respObj);
				return;
			}

			//
			PreparedStatement s = db.getConnection().prepareStatement("SELECT name,role FROM users where userId=?;");
			s.setInt(1, userID);

			ResultSet rs = s.executeQuery();
			if (rs.next()) {
				respObj.put("userId", userID);
				respObj.put("userName", rs.getString(1));
				respObj.put("role", rs.getString(2));
			} else
				throw new RuntimeException("not logged in");

			s.close();
			rs.close();
			String key = request.getParameter("key");
			if (key != null) {
				try {
					int value = addKey(db, key, userID);
					response.getWriter().println("added " + value + " beers!");
				} catch (Exception e) {
					response.getWriter().println(e.getMessage());
				}
			} else {
				String result = checkLastPaymentRcv(db, userID);
				if (result != null)
					respObj.put("lastPaymentRcv", result);
				String dec = request.getParameter("dec");
				if ("1".equals(dec)) {
					s = db.getConnection().prepareStatement("insert into book (userId, value) values (?, -1);");
					s.setInt(1, userID);

					s.execute();
					s.close();

					// check free beer
					Random r = new Random();
					if (r.nextInt(25) == 0) {
						try {
							String promoType = "promotionkey";
							// check promo code available
							s = db.getConnection().prepareStatement(
									"SELECT data FROM keytable WHERE type=? and active=1 LIMIT 0, 1;");
							s.setString(1, promoType);

							rs = s.executeQuery();
							int res = 0;
							if (rs.next())
								res = addKey(db, rs.getString(1), userID, promoType);

							respObj.put("promobeer", res);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				s = db.getConnection().prepareStatement("SELECT SUM(value) FROM book WHERE userId=?;");
				s.setInt(1, userID);

				rs = s.executeQuery();
				if (rs.next())
					respObj.put("credit", rs.getInt(1));

				int resetHour = 17;

				Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
				c.setTimeInMillis(System.currentTimeMillis());
				c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), resetHour, 0, 0);
				Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
				if (now.getTimeInMillis() < c.getTimeInMillis())
					c.setTimeInMillis(c.getTimeInMillis() - 24 * 3600 * 1000);
				java.sql.Timestamp d = new java.sql.Timestamp(c.getTimeInMillis());

				s = db.getConnection().prepareStatement(
						// "select sum(value) from book where (type = 'consume')
						// and timeStamp >= CURDATE() AND timeStamp < CURDATE()
						// + INTERVAL 12 HOUR and userId=?;");

						"select sum(value) from book where (type = 'consume') and timeStamp>= ? and userId=?;");
				System.out.println(d);
				s.setTimestamp(1, d, c);
				s.setInt(2, userID);

				rs = s.executeQuery();
				if (rs.next())
					respObj.put("today", -1 * rs.getInt(1));

				s.close();
				response.getWriter().println(respObj);
			}
		} catch (SQLException e) {
			response.sendError(111);
			throw new RuntimeException("failed: " + e.getMessage(), e);
		} finally {
			if (db != null) {

				db.close();
			}
		}

	}

	private String checkLastPaymentRcv(DBConnection db, Integer userID) {
		try {
			PreparedStatement s = db.getConnection()
					.prepareStatement("SELECT lastPaymentRcvIdx from users where userId=?");
			s.setInt(1, userID);
			ResultSet rs = s.executeQuery();
			int lastIdx = 0;
			if (rs.next())
				lastIdx = rs.getInt(1);
			s.close();
			s = db.getConnection().prepareStatement(
					"SELECT id,value,keyData,timeStamp from book where userId=? and type='payment' and id>? and value>0 order by id desc");
			s.setInt(1, userID);
			s.setInt(2, lastIdx);
			rs = s.executeQuery();

			int sum = 0;
			String keyData = "";
			int id = 0;
			StringBuffer res = new StringBuffer("Recieved: <br>");
			PreparedStatement s2 = db.getConnection().prepareStatement(
					"SELECT book.userId,users.Name from (book join users on((book.userId = users.userId)))  where type='payment' and keyData=?");

			while (rs.next()) {
				int val = rs.getInt(2);
				sum += val;
				id = rs.getInt(1);
				Timestamp ts = new Timestamp(rs.getTimestamp(4).getTime() + 6 * 3600 * 1000);
				keyData = rs.getString(3);
				s2.setString(1, keyData.substring("_COPY_".length()));
				ResultSet rs2 = s2.executeQuery();
				rs2.next();
				res.append(ts + ": " + val + " from user " + rs2.getInt(1) + " " + rs2.getString(2) + "<br>");
			}
			s2.close();
			res.append("sum: " + sum);
			s.close();
			if (id > lastIdx) {
				s = db.getConnection().prepareStatement("UPDATE users SET lastPaymentRcvIdx=? where userId=?");
				s.setInt(1, id);
				s.setInt(2, userID);
				s.execute();
				s.close();
			}
			if (sum > 0)
				return res.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private int addKey(DBConnection db, String key, int userID) {
		return addKey(db, key, userID, "prepaidkey");
	}

	private int addKey(DBConnection db, String key, int userID, String keyType) {
		PreparedStatement s = null;
		PreparedStatement ps = null;
		int value;
		try {
			db.getConnection().setAutoCommit(false);
			s = db.getConnection().prepareStatement(
					"SELECT data,value,active from keytable where active=1 and data=? and type=?",
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
			s.setString(1, key);
			s.setString(2, keyType);
			ResultSet rsKey = s.executeQuery();
			if (!rsKey.next()) {
				throw new RuntimeException("invalid key - no transaction");
			}

			value = rsKey.getInt(2);
			rsKey.updateInt(3, 0);// deactivate key

			ps = db.getConnection()
					.prepareStatement("insert into book (userId, value, type, keyData) values (?, ?, ?, ?);");

			ps.setInt(1, userID);
			ps.setInt(2, value);
			ps.setString(3, "prepay");
			ps.setString(4, key);
			ps.executeUpdate();

			rsKey.updateRow();
			rsKey.close();
			db.getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (db.getConnection().getAutoCommit() == false)
					db.getConnection().rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (s != null)
				try {
					s.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return value;
	}

	protected void d_oGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
		String PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
		String USERNAME = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
		String PASSWORD = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");

		if (HOST == null) {
			HOST = "localhost";
			PORT = "3306";
			USERNAME = "root";
			PASSWORD = "pass";
		}
		response.getWriter().append("xxxxxattttaatsstattt ");
		Connection conn = null;
		Properties connectionProps = new Properties();
		connectionProps.put("user", USERNAME);
		connectionProps.put("password", PASSWORD);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			response.getWriter().append("Error while loading driver: " + e.getMessage());
		}

		try {
			conn = DriverManager.getConnection("jdbc:mysql://" + HOST + ":" + PORT + "/bk", USERNAME, PASSWORD);
			response.getWriter().append(" bindat ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.getWriter().append(e.toString());
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
