package mail;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {
	Properties props = new Properties();

	public Mail() {
		props.put("mail.smtp.host", "mail.gmx.net");
		props.put("mail.smtp.port", "465");
		props.put("mail.from", "bierkassa@gmx.at");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.enable", "true");
	}

	public void send(String subject, String text) {
		try {
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("bierkassa@gmx.at", "Hofstettner71");
				}
			});
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("bierkassa@gmx.at", "Bierkassa"));
			msg.setRecipients(Message.RecipientType.TO, "azesberger@gmx.at");

			msg.setSubject(subject);
			msg.setSentDate(new Date());
			msg.setText(text);

			Transport trnsport;
			trnsport = session.getTransport("smtp");
			trnsport.connect();
			msg.saveChanges();
			trnsport.sendMessage(msg, msg.getAllRecipients());
			trnsport.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
