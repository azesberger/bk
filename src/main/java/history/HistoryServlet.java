package history;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.DBConnection;

/**
 * Servlet implementation class HistoryServlet
 */
@WebServlet("/HistoryServlet")
public class HistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HistoryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if (session == null) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "not logged in");
			return;
		}
		int userID = (Integer) session.getAttribute("userId");
		DBConnection db = null;

		try {
			db = new DBConnection();
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment;filename=history.csv");
			OutputStream os = response.getOutputStream();
			PrintWriter w = new PrintWriter(os);

			try {
				// SELECT * FROM book WHERE userId =1 order by id
				PreparedStatement s = db.getConnection()
						// .prepareStatement("SELECT * FROM book where userId=?
						// order by id desc");
						.prepareStatement(
								"SELECT  book.id,timeStamp,book.value,book.type,KeyData,keytable.value FROM book left join keytable on keytable.data=book.keyData where userId=? order by id desc");
				s.setInt(1, userID);

				Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));

				ResultSet rs = s.executeQuery();

				// group by day (17:00 Uhr)
				PreparedStatement s2 = db.getConnection().prepareStatement(
						"SELECT cast(book.timeStamp-interval 11 hour as date) as t,sum(if((book.value < 0),-(book.value),0)),sum(if((book.value > 0),book.value,0)) from book where userId=? and ((book.type='consume') or (book.type='prepay')) group by cast(book.timeStamp-interval 11 hour as date) order by t desc");
				s2.setInt(1, userID);
				ResultSet rs2 = s2.executeQuery();

				// group by week
				PreparedStatement s3 = db.getConnection().prepareStatement(
						"SELECT yearweek(book.timeStamp,0) as t,sum(if(( book.value  < 0),-( book.value),0)) ,sum(if((book.value > 0), book.value ,0))  from  book where userId=? and ((book.type='consume') or (book.type='prepay')) group by yearweek(book.timeStamp ,0) order by t desc");
				s3.setInt(1, userID);
				ResultSet rs3 = s3.executeQuery();
				w.println("Year/Week;out;in;;day;out;in;;TransactionId;Timestamp;Value;Type;KeyData;KeyPrice(€)");
				DateFormat converter = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
				converter.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
				while (rs.next()) {
					if (rs3.next()) {
						w.print(rs3.getString(1) + ";" + rs3.getInt(2) + ";" + rs3.getInt(3) + ";;");
					} else
						w.print(";;;;");
					if (rs2.next()) {
						w.print(rs2.getDate(1) + ";" + rs2.getInt(2) + ";" + rs2.getInt(3) + ";;");
					} else
						w.print(";;;;");

					Timestamp ts = rs.getTimestamp(2, c);
					c.setTime(ts);

					w.println(rs.getInt(1) + ";" + converter.format(c.getTime()) + ";" + rs.getInt(3) + ";"
							+ rs.getString(4) + ";" + rs.getString(5) + ";" + rs.getInt(6));
				}
				s.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
				w.println(e);
			}

			w.flush();
			w.close();

		} finally {
			if (db != null) {

				db.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
