package pay;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import admin.CreateCodesServlet;
import db.DBConnection;

/**
 * Servlet implementation class PayServlet
 */
@WebServlet("/PayServlet")
public class PayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DBConnection db;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PayServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		if (session == null) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "not logged in");
			return;
		}
		int userID = (Integer) session.getAttribute("userId");
		response.setContentType("application/json");

		JSONObject respObj = new JSONObject();
		try {
			db = new DBConnection();
			db.getConnection().setAutoCommit(false);

			String getUserId = null;
			int userIdTo = 0;
			String text = "error - no transaction";
			String userNameTo = "user not found";
			if ((getUserId = request.getParameter("getUser")) != null) {
				PreparedStatement s = db.getConnection().prepareStatement("SELECT name FROM users where userId=?;");
				userIdTo = Integer.parseInt(getUserId);
				s.setInt(1, userIdTo);
				ResultSet rs = s.executeQuery();
				if (rs.next())
					userNameTo = rs.getString(1);

				respObj.put("userName", userNameTo);
			}
			String value = null;
			if ((value = request.getParameter("value")) != null) {
				int val = Integer.parseInt(value);
				if (val > 0) {
					if (pay(userID, userIdTo, val))
						text = "payed " + val + " to user " + userIdTo + " " + userNameTo;

					respObj.put("pay", new Integer(1));
					respObj.put("text", text);
				}
			}

			response.getWriter().println(respObj);
		} catch (SQLException e) {
			response.sendError(111);
			throw new RuntimeException("failed: " + e.getMessage(), e);
		} finally {
			if (db != null) {
				try {
					db.getConnection().setAutoCommit(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				db.close();
			}
		}
	}

	private boolean pay(int userID, int userIdTo, int val) {
		if (userID == 0)
			return false;
		if (userIdTo == 0)
			return false;
		if (val <= 0)
			return false;
		try {
			PreparedStatement s = db.getConnection()
					.prepareStatement("INSERT INTO keytable (type, data, value, price) VALUES (?, ?, ?, ?);");
			String key = CreateCodesServlet.createEncodedID();
			s.setString(1, "payment");
			s.setString(2, key);
			s.setInt(3, val);
			s.setInt(4, val);

			s.executeUpdate();
			s.close();

			s = db.getConnection()
					.prepareStatement("insert into book (userId, value, type, keyData) values (?,?,?,?);");
			s.setInt(1, userID);
			s.setInt(2, -val);
			s.setString(3, "payment");
			s.setString(4, key);

			s.execute();
			s.close();

			s = db.getConnection()
					.prepareStatement("insert into book (userId, value,type, keyData) values (?, ?,?,?);");
			s.setInt(1, userIdTo);
			s.setInt(2, val);
			s.setString(3, "payment");
			s.setString(4, "_COPY_" + key);

			s.execute();
			s.close();

			db.getConnection().commit();
			System.out.println("pay from " + userID + " to " + userIdTo + " value " + val);
			return true;
		} catch (SQLException e) {
			try {
				db.getConnection().rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
